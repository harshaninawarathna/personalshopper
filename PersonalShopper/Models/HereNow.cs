﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalShopper.Models
{
    public class HereNow
    {
        public int count { get; set; }
        public string summary { get; set; }
        public List<object> groups { get; set; }
    }
}