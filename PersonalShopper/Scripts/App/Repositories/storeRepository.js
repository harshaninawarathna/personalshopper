﻿storeModule.factory('storeRepository', function ($http, $q) {
    return {
        get : function (latitude, longitude,query) {
            var deffered = $q.defer();
            $http.get('/api/Store?latitude=' + latitude + '&longitude=' + longitude + '&query=' + query)
                .success(deffered.resolve)
                .error(deffered.reject);
            return deffered.promise;
        }
        
    };
});