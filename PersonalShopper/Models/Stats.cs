﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalShopper.Models
{
    public class Stats
    {
        public int checkinsCount { get; set; }
        public int usersCount { get; set; }
        public int tipCount { get; set; }
    }
}