﻿storeModule.controller("storeController", function ($scope, storeRepository) {
    $scope.longitude = '79.9008';
    $scope.latitude = '6.7964';
    $scope.query = 'grocery';
    $scope.stores = [];

    $scope.onStoreFindClick = function() {
        storeRepository.get($scope.latitude, $scope.longitude, $scope.query).then(function(list) {
            $scope.stores = list;
        });
    };

    $scope.storesGrid = {
        data: 'stores',
        multiSelect: false,
        enableColumnResize: true,
        enableColumnReordering: false,
        selectedItems: $scope.selectedRow,
        columnDefs: [{ field: "Id", displayName:'Id', visible:false},
            { field: "Name", displayName: 'Name' },
            { field: "Distance", displayName: 'Distance in meters' },
             { field: "Latitude", displayName: 'Latitude' },
             { field: "Longitude", displayName: 'Longitude' },
             { field: "Address", displayName: 'Address' }
        ]
            
    };

});