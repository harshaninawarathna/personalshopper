﻿var storeModule = angular.module('store', ['ngRoute', 'ngGrid']);

storeModule.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/store', {
            templateUrl: '/Templates/Store/Store.html',
            controller: 'storeController'
        })
        .otherwise({ redirectTo: '/store' });

}]);