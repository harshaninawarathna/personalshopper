﻿namespace PersonalShopper.Models
{
    public class UserInput
    {
        public int Id { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Category { get; set; }
    }
}