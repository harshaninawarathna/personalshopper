using System.Collections.Generic;
using PersonalShopper.Models;

namespace PersonalShopper.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<PersonalShopper.Models.PersonalShopperContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PersonalShopperContext context)
        {
            var userInput = new List<UserInput>
                               {
                                   new UserInput()
                                       {
                                           Id = 1,
                                           Longitude = "80",
                                           Latitude = "6",
                                           Category = "Grocery"
                                       },

                                       new UserInput()
                                       {
                                           Id = 1,
                                           Longitude = "80",
                                           Latitude = "6",
                                           Category = "Cake"
                                       }
                                   
                               };
            userInput.ForEach(c => context.UserInput.AddOrUpdate(i => i.Id, c));
            context.SaveChanges();
        }
    }
}
