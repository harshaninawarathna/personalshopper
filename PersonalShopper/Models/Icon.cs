﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalShopper.Models
{
    public class Icon
    {
        public string prefix { get; set; }
        public string suffix { get; set; }
    }
}