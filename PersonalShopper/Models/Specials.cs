﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalShopper.Models
{
    public class Specials
    {
        public int count { get; set; }
        public List<object> items { get; set; }
    }
}