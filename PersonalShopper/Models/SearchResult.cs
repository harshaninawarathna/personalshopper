﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalShopper.Models
{
    public class SearchResult
    {
        public Meta meta { get; set; }
        public Response response { get; set; }
    }
}