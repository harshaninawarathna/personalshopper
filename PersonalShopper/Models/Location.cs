﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalShopper.Models
{
    public class Location
    {
        public string address { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
        public int distance { get; set; }
        public string postalCode { get; set; }
        public string cc { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public List<string> formattedAddress { get; set; }
        public string crossStreet { get; set; }
        public bool? isFuzzed { get; set; }
    }
}