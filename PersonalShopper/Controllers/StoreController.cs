﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json;
using PersonalShopper.Models;

namespace PersonalShopper.Controllers
{
    public class StoreController : ApiController
    {
        private PersonalShopperContext db = new PersonalShopperContext();
        private string CLIENT_ID = "EV2D5WZ5VRHUZU21JJTMEZE1CSKJZSE05NYUXONQYRVP1EA5";
        private string CLIENT_SECRET = "ZUTYHMVHM3YN52YT2QBBPWO2C0YZ4XPJZ0IFCK1LYSJJJUYZ";
        private string LATITUDE = "6.9344";
        private string LONGITUDE = "79.8428";
        private string QUERY = "Grocery";

        public StoreController()
        {
            
        }

        public StoreController(PersonalShopperContext context)
        {
            db = context;
        }

        // GET api/Store
        public List<Store> GetStores()
        {
            //return db.Stores;
            var client = new WebClient();
            var url = "https://api.foursquare.com/v2/venues/search?client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&v=20130815&ll=" + LATITUDE + "," + LONGITUDE + "&query=" + QUERY;
            var jsonResult = client.DownloadString(url);
            var result = JsonConvert.DeserializeObject<SearchResult>(jsonResult);

            var stores = new List<Store>();

            if (result != null)
            {
                var venues = result.response.venues;
                int i = 1;

                foreach (var venue in venues)
                {
                    var store = new Store()
                                    {
                                        Id = i,
                                        Name = venue.name,
                                        Distance = venue.location.distance,
                                        Latitude = venue.location.lat,
                                        Longitude = venue.location.lng
                                    };

                    if (venue.location.address != null )
                    {
                        foreach (var adressLine in venue.location.address)
                        {
                            store.Address += adressLine + ", ";
                        }
                    }

                    stores.Add(store);
                    i++;
                }
            }

            return stores;
        }

        // GET api/Store?latitude=&longitude=&query=
        public IList<Store> GetStores(string latitude, string longitude,string query)
        {
            
            try
            {
                //saving user input for future references
                var userInput = new UserInput()
                                    {
                                        Latitude = latitude,
                                        Longitude = longitude,
                                        Category = query,
                                    };
                db.UserInput.Add(userInput);
                db.SaveChanges();

            }catch(Exception)
            {
                //do nothing
            }

            var client = new WebClient();
            var url = "https://api.foursquare.com/v2/venues/search?client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&v=20130815&ll=" + latitude + "," + longitude + "&query=" + query;
            var jsonResult = client.DownloadString(url);
            var result = JsonConvert.DeserializeObject<SearchResult>(jsonResult);

            var stores = new List<Store>();

            if (result != null)
            {
                var venues = result.response.venues;
                int i = 1;

                foreach (var venue in venues)
                {
                    var store = new Store()
                    {
                        Id = i,
                        Name = venue.name,
                        Distance = venue.location.distance,
                        Latitude = venue.location.lat,
                        Longitude = venue.location.lng
                    };

                    if (venue.location.address != null)
                    {
                        foreach (var adressLine in venue.location.address)
                        {
                            store.Address += adressLine + ", ";
                        }
                    }

                    stores.Add(store);
                    i++;
                }
            }

            return stores.OrderBy(x => x.Distance).ToList();
        }

        // GET api/Store/5
        [ResponseType(typeof(Store))]
        public IHttpActionResult GetStore(int id)
        {
            Store store = db.Stores.Find(id);
            if (store == null)
            {
                return NotFound();
            }

            return Ok(store);
        }

        // PUT api/Store/5
        public IHttpActionResult PutStore(int id, Store store)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != store.Id)
            {
                return BadRequest();
            }

            db.Entry(store).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StoreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Store
        [ResponseType(typeof(Store))]
        public IHttpActionResult PostStore(Store store)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Stores.Add(store);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = store.Id }, store);
        }

        // DELETE api/Store/5
        [ResponseType(typeof(Store))]
        public IHttpActionResult DeleteStore(int id)
        {
            Store store = db.Stores.Find(id);
            if (store == null)
            {
                return NotFound();
            }

            db.Stores.Remove(store);
            db.SaveChanges();

            return Ok(store);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StoreExists(int id)
        {
            return db.Stores.Count(e => e.Id == id) > 0;
        }
    }
}